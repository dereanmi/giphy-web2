import React, { Component } from 'react';
import './App.css';
import Routes from './Containers/Routes'
// import  'antd/dist/antd.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Routes/>
      </div>
    );
  }
}

export default App;
