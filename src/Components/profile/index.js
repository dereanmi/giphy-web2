import React, { Component } from 'react';
import { Modal, Button } from 'antd';
import { auth } from '../../firebase';
const KEY_USER_DATA = 'user_data';
// function getEmail() {
//     const jsonStr = localStorage.getItem(KEY_USER_DATA);
//     return JSON.parse(jsonStr).email;
// }

class Profile extends Component {
    state = {
        email: '',
        isShowDialog: false
    }
    componentDidMount() {
        const jsonStr = localStorage.getItem(KEY_USER_DATA)
        this.setState({ email: JSON.parse(jsonStr).email })
    }
    showDialogConfirmLogout = () => {
        this.setState({ isShowDialog: true })
    }
    handleCancel = () => {
        this.setState({ isShowDialog: false })
    }


    handleOk = () => {
        localStorage.setItem(KEY_USER_DATA, JSON.stringify(
            {
                isShowDialog: false
            }
        ))
        this.props.history.push('/')
    }


    render() {
        return (
            <div>
                <h1>Email : {this.state.email}</h1>
                <Button
                    type="primary"
                    loading={this.state.isShowDialog}
                    onClick={this.showDialogConfirmLogout}>
                    Logout
            </Button>

                <Modal
                    title="Confirm"
                    visible={this.state.isShowDialog}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <p>Are you sure to logout?</p>

                </Modal>
            </div >
        );
    }
}


export default Profile;

